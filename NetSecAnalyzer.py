from scapy.all import *

# Función para analizar los paquetes capturados
def analyze_packet(packet):
    if IP in packet:  # Verificar si es un paquete IP
        ip_packet = packet[IP]
        src_ip = ip_packet.src  # Dirección IP de origen
        dst_ip = ip_packet.dst  # Dirección IP de destino

        if ICMP in ip_packet:  # Verificar si es un paquete ICMP
            icmp_packet = ip_packet[ICMP]
            # Obtener el tipo de mensaje ICMP
            icmp_type = icmp_packet.type
            # Detectar un posible ataque de ping flood
            detect_icmp_flood(src_ip, dst_ip, icmp_type)
        elif TCP in ip_packet:  # Verificar si es un paquete TCP
            tcp_packet = ip_packet[TCP]
            # Obtener puertos de origen y destino
            src_port = tcp_packet.sport
            dst_port = tcp_packet.dport
            # Detectar un posible escaneo de puertos
            detect_port_scan(src_ip, dst_ip, src_port, dst_port)
            # Detectar paquetes de un tamaño inusualmente grande
            packet_length = len(packet)
            detect_large_packets(src_ip, dst_ip, packet_length)

# Función para detectar un posible ataque de ping flood
def detect_icmp_flood(src_ip, dst_ip, icmp_type):
    if icmp_type == 8:  # Si el tipo de mensaje ICMP es ICMP Echo Request
        print(f"Posible ataque de ping flood detectado desde {src_ip} hacia {dst_ip}.")

# Función para detectar un posible escaneo de puertos
def detect_port_scan(src_ip, dst_ip, src_port, dst_port):
    if dst_port == 80 or dst_port == 443:  # Si el puerto de destino es 80 (HTTP) o 443 (HTTPS)
        print(f"Posible escaneo de puertos en los puertos 80 o 443 desde {src_ip}:{src_port} hacia {dst_ip}:{dst_port}.")

# Función para detectar paquetes de un tamaño inusualmente grande
def detect_large_packets(src_ip, dst_ip, packet_length):
    if packet_length > 1500:  # Si el tamaño del paquete es mayor que 1500 bytes
        print(f"Paquete TCP inusualmente grande detectado desde {src_ip} hacia {dst_ip}. Posible ataque de inundación de red.")

# Función principal
def main():
    print("Escuchando paquetes...")
    # Capturar paquetes en tiempo real y pasarlos a la función analyze_packet
    sniff(prn=analyze_packet, store=0)

if __name__ == "__main__":
    main()
