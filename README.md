# Instrucciones para Configurar el Entorno Virtual y Ejecutar el Código

## Crear y Activar un Entorno Virtual con Python

1. Abre una terminal.
2. Instala la herramienta `virtualenv`, si aún no la tienes:
    ```bash
    pip install virtualenv
    ```
3. Crea un nuevo directorio para tu proyecto y navega a él:
    ```bash
    mkdir NetSecAnalyzer
    cd NetSecAnalyzer
    ```
4. Crea un entorno virtual dentro del directorio del proyecto:
    ```bash
    virtualenv venv
    ```
5. Activa el entorno virtual:
    - En Windows:
        ```bash
        venv\Scripts\activate
        ```
    - En macOS y Linux:
        ```bash
        source venv/bin/activate
        ```

## Instalar Dependencias del Código

1. Una vez activado el entorno virtual, instala las dependencias ejecutando:
    ```bash
    pip install scapy
    ```

## Objetivo General del Código:
El objetivo general del código es desarrollar una herramienta de análisis de tráfico de red que detecte patrones maliciosos en los paquetes capturados. La herramienta debe ser capaz de ejecutarse en sistemas Windows y utilizar la biblioteca Scapy para capturar y analizar los paquetes en tiempo real. Al detectar un patrón malicioso, la herramienta imprimirá información relevante sobre el paquete, incluyendo direcciones IP de origen y destino, así como puertos en el caso de paquetes TCP.

## Explicación de los Tres Patrones Analizados:
1. **Ataque de Ping Flood:** Un ataque de ping flood es una forma de ataque de denegación de servicio (DoS) en el cual el atacante envía una gran cantidad de solicitudes de eco ICMP (ping) a la víctima, abrumando su capacidad de respuesta y causando una interrupción en el servicio. Al detectar mensajes ICMP Echo Request (tipo 8) en grandes cantidades, se puede sospechar la presencia de un ataque de ping flood.

2. **Posible Escaneo de Puertos:** Un escaneo de puertos es una técnica utilizada por los atacantes para descubrir qué puertos están abiertos en un sistema, lo que les permite identificar posibles puntos de entrada para realizar ataques más específicos. Detectar una alta actividad en los puertos 80 (HTTP) o 443 (HTTPS) puede indicar un escaneo de puertos en busca de servicios web.

3. **Paquetes de Tamaño Inusualmente Grande:** Los paquetes de un tamaño inusualmente grande pueden indicar un intento de inundación de red, donde el atacante envía una gran cantidad de datos con el fin de sobrecargar la capacidad de la red y causar una interrupción en el servicio. Detectar paquetes TCP de gran tamaño puede ser una señal de un posible ataque de inundación de red en curso.

## Explicación del Código:
El código utiliza la biblioteca Scapy para capturar y analizar paquetes de red en tiempo real. Para cada paquete capturado, se realizan tres análisis para detectar patrones maliciosos: ataque de ping flood, posible escaneo de puertos y paquetes de tamaño inusualmente grande. Cuando se detecta un patrón malicioso, se imprime información detallada sobre el paquete, incluyendo direcciones IP de origen y destino, así como puertos en el caso de paquetes TCP.


Para ejecutar el código, simplemente activa el entorno virtual y ejecuta el script `NetSecAnalyzer.py`.

